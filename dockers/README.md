# Various dockers used in my VPS

## Clean starter docker

```bash
docker pull alpine:latest
docker pull bitnami/minideb:latest

docker run -it --rm --name BASE_ALPINE alpine:latest
docker run -it --rm --name BASE_MINIDEB bitnami/minideb:jessie
```

## Useful links

https://dzone.com/articles/minideb-a-minimalist-debian-based-docker-image

https://www.ctl.io/developers/blog/post/optimizing-docker-images/
