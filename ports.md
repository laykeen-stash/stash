# Ports used for outgoing traffic by service

| Ports | Service | Optional/TODO |
| :--- | :--- | :--- |
| 21 | FTP | |
| 22 | SSH | |
| 80 | HTTP | Redirect to 443 |
| 443 | HTTPS | **Every web service should go here** |
| 25575 | Minecraft | mc docker |
| 25565 | Minecraft alt | mc docker alternative port |


## Ports used internally and they should NOT GO OUT

| Ports | Service | Optional/TODO |
| :--- | :---| :--- |
| 7090 | Leantime | redirect to https://lean.x.x |
| 7091 | Leantime port 9000 | **ONLY LOCAL NETWORK** |
| 3306 | Leantime db | TODO: think about this |
| 9000 | Portainer | redirect to https://portainer.x.x |
