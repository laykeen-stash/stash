# Laykeen_stash

Personal website

## Used and useful commands

```bash
#basic stuff
sudo hostnamectl set-hostname vpsname


#ssh config (see ### my sshd_config)
sudo nano /etc/ssh/sshd_config


#fail2ban
sudo apt update && sudo apt upgrade
sudo apt install fail2ban
sudo nano /var/log/auth.log
sudo nano /etc/fail2ban/jail.conf # modify as hardly you want
sudo systemctl restart fail2ban
sudo fail2ban-client status
sudo fail2ban-client status sshd


#docker
sudo apt install docker.io
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose -v
curl https://raw.githubusercontent.com/jesseduffield/lazydocker/master/scripts/install_update_linux.sh | bash


#wireguard
git clone https://github.com/Nyr/wireguard-install
cd wireguard-install
chmod +x wireguard-install.sh
./wireguard-install.sh


#ufw
sudo apt install ufw
sudo ufw allow ssh
sudo ufw allow www
sudo ufw allow ftp
sudo ufw allow 51820 #wireguard
sudo ufw enable


#lets encrypt
sudo apt update
sudo apt install python-certbot-apache
sudo nano /etc/apache2/sites-available/000-default.conf #messo x.x al posto di www.example.com
sudo apache2ctl configtest
sudo systemctl reload apache2
sudo certbot --apache -d x.x -d www.x.x #email, A, N, 2 ENTER
# dry run is a test and DOES NOT renew or create certificates
sudo certbot renew --dry-run
sudo letsencrypt


#shell (see ### Powerline-shell into .bashrc)
sudo -H pip install powerline-shell


#https://echosystem.fr/articles/monitoring/fail2web#access-it-via-apache-next-we-need-to-create-a-vhost-config-for-fail2web
#fail2web
sudo apt-get install golang git gcc
go get github.com/Sean-Der/fail2rest
export GOPATH=$HOME/go
cd $GOPATH/src/github.com/Sean-Der/fail2rest
sudo git clone --depth=1 https://github.com/Sean-Der/fail2web.git /var/www/fail2web
sudo htpasswd -cB /var/www/.htpasswd $USER
sudo a2enmod proxy proxy_ajp proxy_http rewrite deflate headers proxy_balancer proxy_connect proxy_html
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/fail2web.conf
sudo nano /etc/apache2/sites-available/fail2web.conf #see (### fail2web apache conf)
sudo a2ensite fail2web.conf
sudo systemctl restart apache2
sudo systemctl status apache2
sudo ln -s $GOPATH/bin/fail2rest /usr/bin/
sudo cp $GOPATH/src/github.com/Sean-Der/fail2rest/config.json /etc/fail2rest.json
sudo ln -s $GOPATH/src/github.com/Sean-Der/fail2rest/init-scripts/systemd /etc/systemd/system/fail2rest.service
#oppure
sudo cp $GOPATH/src/github.com/Sean-Der/fail2rest/init-scripts/debian.sh fail2rest
/etc/init.d/fail2rest start
/etc/init.d/fail2rest status
/etc/init.d/fail2rest stop
sudo systemctl start fail2rest


#portainer
# https://markus-blog.de/index.php/2018/05/11/how-to-manage-docker-with-portainer-on-ubuntu-16-04-lts-with-apache2-as-reverse-proxy/
docker volume create portainer_data
docker run -d -p 9000:9000 --restart always -v /var/run/docker.sock:/var/run/docker.sock -v /opt/portainer:/data portainer/portainer
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/portainer.conf
sudo nano /etc/apache2/sites-available/portainer.conf #see (### portainer conf)
sudo a2ensite portainer.conf
sudo systemctl restart apache2
sudo certbot --apache #portainer.x.x
sudo nano /etc/apache2/sites-available/portainer-le-ssl.conf #see (#### portainer apache conf under SSL)
sudo systemctl restart apache2
#register an admin account


#leantime
git clone https://github.com/Leantime/docker-leantime.git
cd docker-leantime/
git checkout -b personal_use_pull
nano docker-compose.yml #see (### Leantime compose)
docker-compose up -d
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/leantime.conf
sudo certbot --apache
sudo nano /etc/apache2/sites-available/leantime-le-ssl.conf #like portainer conf
sudo systemctl restart apache2
# TODO fix api redirect and missing jquery


#cryptpad
git clone https://github.com/xwiki-labs/cryptpad-docker
cd cryptpad-docker
sudo docker-compose up
#apache config: https://github.com/xwiki-labs/cryptpad/wiki/Apache
sudo chown -R 4001:4001 cryptpad
```

## Used and useful setups

### Powerline-shell into .bashrc

```sh
function _update_ps1() {
    PS1="$(powerline-shell $? 2> /dev/null)"
}

if [ "$TERM" != "linux" ]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi
```


### Powerline-shell into zsh

#### clone powerline
```sh
git clone https://github.com/powerline/powerline
```
#### add plugin into .zshrc
```sh
. {cloned_repo_path}/powerline/powerline/bindings/zsh/powerline.zsh
```

### My sshd_config

```bash
ClientAliveInterval 300
ClientAliveCountMax 3

#Yeah I am a bit slow sometimes
Match User myusername
        ClientAliveInterval 600
```

### fail2web apache conf

```xml
<VirtualHost *:80>  
    ServerName fail2web.serverDNSname.domain
    ServerAlias www.fail2web.serverDNSname.domain
    DocumentRoot /var/www/fail2web/web

    <Location />  
        AuthType Basic  
        AuthName "Restricted"  
        AuthBasicProvider file  
        AuthUserFile /var/www/.htpasswd  
        Require valid-user  

    </Location>  

    ProxyPass /api http://127.0.0.1:5000  

</VirtualHost>
```

### personalized MOTD
execute a scripted program at login:<br/>
**/etc/update-motd.d/20-users**
```sh
#!/bin/sh
printf "Users online: \033[33m$(users)\033[0m\n"
# output all current logged in users
```



### portainer apache conf

```xml
<VirtualHost *:80>  
    ServerName portainer.serverDNSname.domain
    ServerAlias www.portainer.serverDNSname.domain

    #RewriteEngine on
    #RewriteCond %{SERVER_NAME} =www.portainer.x.x [OR]
    #RewriteCond %{SERVER_NAME} =portainer.x.x
    #RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
```

#### portainer apache conf under SSL

```xml
<IfModule mod_ssl.c>
    <VirtualHost *:443>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        ServerName portainer.x.x
        ServerAlias www.portainer.x.x
        ServerAdmin webmaster@localhost


        ProxyPass / http://127.0.0.1:9000/
        ProxyPassReverse / http://127.0.0.1:9000/
        RequestHeader set X-Forwarded-Proto "https"

        ProxyVia Block

        <Proxy *>
                Require all granted
        </Proxy>
        # SSL Configuration - uses strong cipher list - these might need to be downgraded if you need to support older browsers/devices
        SSLEngine on
        SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH
        SSLProtocol All -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
        SSLHonorCipherOrder On

        # HSTS (optional)
        Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains;"
        # Prevent MIME based attacks
        Header set X-Content-Type-Options "nosniff"

        ErrorLog /var/log/apache2/portainer-error.log
        CustomLog /var/log/apache2/portainer-access.log combined

        #Proxy to docker container console
        <Location /api/websocket/>
                RewriteEngine on
                RewriteCond %{HTTP:UPGRADE} ^WebSocket$ [NC]
                RewriteCond %{HTTP:CONNECTION} Upgrade$ [NC]
                RewriteRule /api/websocket/(.*) ws://127.0.0.1:9000/api/websocket/$1 [P]
        </Location>

        SSLCertificateFile /etc/letsencrypt/live/portainer.x.x/fullchain.pem
        SSLCertificateKeyFile /etc/letsencrypt/live/portainer.x.x/privkey.pem
        Include /etc/letsencrypt/options-ssl-apache.conf
    </VirtualHost>
</IfModule>
```


## Repo management

### docker experiments

[docker compose](https://docs.docker.com/compose/)

[alpine docker](https://hub.docker.com/_/alpine)

```sh
docker pull alpine:latest
```
```sh
# packet manager APK
apk update
# service manager
apk add openrc
rc-service start {service}
```

```sh
docker run -it -p 8080:80 {docker_name}
#       docker out ^   ^ docker virtual in
# Run a docker in iterative console
```

### Leantime compose

```yml
version: '3.3'

services:
   db_leantime:
     image: mysql:5.7
     container_name: mysql_leantime
     volumes:
       - db_data:/var/lib/mysql
     restart: always
     environment:
         MYSQL_ROOT_PASSWORD: 'CHANGEPSWD'
         MYSQL_DATABASE: 'leantime'
         MYSQL_USER: 'admin'
         MYSQL_PASSWORD: 'CHANGEPSWD'
     ports:
       - "3306:3306"
     command: --character-set-server=utf8 --collation-server=utf8_unicode_ci
   web_leantime:
     image: leantime/leantime:latest
     container_name: leantime
     environment:
         LEAN_DB_HOST: 'mysql_leantime'
         LEAN_DB_USER: 'admin'
         LEAN_DB_PASSWORD: 'CHANGEPSWD'
         LEAN_DB_DATABASE: 'leantime'
     ports:
       - "7091:9000"
       - "7090:80"
     depends_on:
       - db_leantime
volumes:
    db_data: {}
```

[leantime](https://hub.docker.com/r/leantime/leantime)



---
---
---



## Server's services setups (SsSsSs)

> Every service has to be a virtual host redirected to port 80/443, expections are made for services that REQUIRE a port (like vpn or some games)

Notice: on urls there are x.x that are your domain and country code

### Actual main server

- apache2
    - port: 80, 443
    - url: x.x
- fail2ban
    - fail2web.x.x
- wireguard
    - port: 51820
    - wireguard.x.x (TODO: ask for registration)

---

### Service server (TODO migrate from Actual main server)

#### Services list

- [ ] homepage
    - [ ] personal blog
    - [ ] projects page
    - [ ] art gallery
    - [ ] CV-like contacts page
- [ ] fail2web
- [x] wireguard
- [ ] syncthing
- [ ] mdc website
- [x] minecraft server
- [ ] terraria server **?**
- [x] leantime

#### Proxy for docker-ports management

- ngnix proxy
    - [nginx-proxy](https://github.com/nginx-proxy/nginx-proxy)
    - ports: 80, 443
    - url: x.x
    - services:
        - homepage (index)
            - contacts
            - projects
            - licenses
        - fail2web
        - letsencrypt

- openssh
    - port: 22

#### Dockerized game servers

Range from 6000 to 6999 (more exeptions here than my standard)

- minecraft
    - port: default for minecraft idk
    - crontab -e
        ```py
        30 7 * * * docker start mc # start the server at 7:30 am
        30 23 * * * docker stop mc # stop the server at 11:30 pm
        ```

#### Dockerized services

Range from 7000 to 7999

- wireguard
    - port: 51820
    - wireguard.x.x (TODO: ask for registration)

- syncthing
    - TODO
    - port: 7080

#### Dockerized web applications

Range from 8000 to 8999 (no exeptions here)
    - from 8000 to 8099: personal stuff
    - from 8100 to 8499: external projects (like others websites)
    - from 8500 to 8999: other stuff

- ror (ruby on rails)
    - services:
        - museum of pcs website
        - museum of pcs database
    - params:
        - `-p 8100:80`
    - port: 8010

- framework X
    - services:
        - showcase (how i know how to use x framework, yeah, that's sad)
    - port: 8500-8999

- blog
    - TODO
    - port: 8000
